import tensorflow as tf
import purelung.auxiliary as auxiliary
import numpy as np
import cv2


class Suppression:
    """Class used for bone suppression in the Chest X-ray images."""

    def __init__(self):
        """
        Load the trained model and create new tensorflow session.
        """
        self.graph = Suppression.load_graph(auxiliary.get_absolute_path('unet_resnet1024.pb')[0])
        self.session = tf.compat.v1.Session(graph=self.graph)

    def predict(self, image, equalize_out=False):
        """
        Perform bone suppression in an input image.

        :param image: an image, in which ribs should be suppressed.
        :param equalize_out: a flag signalizing that the Histograms Equalization should be performed.
        :return: an image, in which bone should be suppressed.
        """
        # create an array to store modified image
        img = np.copy(image)
        img_shape = img.shape
        if img.shape[0] % 8 != 0 or img.shape[1] % 8 != 0:
            new_shape = ((img.shape[0] // 8 + 1) * 8, (img.shape[1] // 8 + 1) * 8)
            img_temp = np.zeros(new_shape, dtype=np.uint8)
            img_temp[:img_shape[0], :img_shape[1]] = img
            img = img_temp
        img = img.reshape((1, 1, 1024, 1024))
        # get the result of suppression
        pred = self.session.run("Relu_26:0", feed_dict={"input.1:0": img})
        res = pred[0, 0, :img_shape[0], :img_shape[1]].numpy()
        res = np.clip(np.round(res), 0, 255).astype(np.uint8)
        # perform the Histogram Equalization over the image
        if equalize_out:
            res = cv2.equalizeHist(res.astype(np.uint8))
        return res

    @staticmethod
    def load_graph(graph_path):
        """Load frozen TensorFlow graph."""
        with tf.io.gfile.GFile(graph_path, "rb") as graph_file:
            graph_definition = tf.compat.v1.GraphDef()
            graph_definition.ParseFromString(graph_file.read())
        graph = tf.Graph()
        with graph.as_default():
            tf.import_graph_def(graph_definition, name="")
        return graph
