import torch
import torchvision


def read_onnx():
    onnx = open('model_weights/retina_net.onnx', encoding='')
    onnx = onnx.readlines()


def segmentation():
    # prepare image
    # parse tf model weights
    model = torchvision.models.resnet50()
    for param_tensor in model.state_dict():
        print(param_tensor, "\t", model.state_dict()[param_tensor])
