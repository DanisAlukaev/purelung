import torch.onnx
import tf2onnx
import onnx
from purelung.models.segmentation import segmentation
from purelung.models.suppression import bone_suppression_pt as suppression
from onnx_tf.backend import prepare


def tf2onnx_func():
    # load the TensorFlow file
    segmentation_model = segmentation.Segmentation()
    session = segmentation_model.session
    # generate the ONNX graph
    onnx_graph = tf2onnx.tfonnx.process_tf_graph(session.graph, input_names=["data:0"],
                                                 output_names=["sigmoid/Sigmoid:0"],
                                                 opset=11)
    # export the TensorFlow model to ONNX
    model_proto = onnx_graph.make_model("test")
    with open("C:/Users/pc/Desktop/purelung/purelung/models/segmentation/model_weights/retina_net.onnx", "wb") as f:
        f.write(model_proto.SerializeToString())


def pth2onnx():
    dummy_input = torch.randn(1, 1, 1024, 1024, device='cpu')
    # load the PyTorch file
    model = suppression.load_bone_model_pytorch()
    # export the PyTorch model to ONNX
    torch.onnx.export(model, dummy_input, "suppression/model_weights/unet_resnet1024.onnx")


def onnx2tf():
    print(1)
    # load the ONNX file
    model = onnx.load('suppression/model_weights/unet_resnet1024.onnx')
    print(2)
    # import the ONNX model to TensorFlow
    tf_rep = prepare(model)
    print(3)
    tf_rep.export_graph('suppression/model_weights/unet_resnet1024.pb')
    print(4)


def main():
    tf2onnx_func()


if __name__ == '__main__':
    main()
