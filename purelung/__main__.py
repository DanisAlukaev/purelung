import purelung.augmentations as aug
from purelung import auxiliary as aux
import cv2
import numpy as np


def main():
    # chest_img = aux.read_dcm(aux.get_absolute_path('image.dcm')[0])
    image = cv2.imread(r'C:\Users\pc\Desktop\4.png', 0)
    # chest_img = np.pad(chest_img, pad_width=500, mode='constant', constant_values=182)
    # aux.save_file_png(chest_img, 'source', 'modified')
    # aux.show_image(chest_img)
    # chest_img = aux.read_dcm(aux.get_absolute_path('img1.dcm')[0])
    augmentation = aug.LungCrop()
    augmented = aux.show_augmented(augmentation, image)


if __name__ == '__main__':
    main()
