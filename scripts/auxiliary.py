from dicompylercore import dicomparser
import matplotlib.pyplot as plt
import pydicom
from pydicom.pixel_data_handlers.util import apply_modality_lut, apply_voi_lut
import numpy as np
import cv2
import os

CONST_WORK_DIR = os.getcwd()


def get_absolute_root_path():
    """
    The function used to get absolute path to a package.

    :return: an absolute path to a package
    """
    os.chdir(CONST_WORK_DIR)
    os.chdir(os.path.dirname(os.getcwd()))
    return os.path.abspath(os.path.curdir)


def save_file_png(image, directory, name):
    """
    The function used to save images.

    :param image: an image to be saved.
    :param directory: a name of directory in '.\purelung\output\'.
    :param name: a name of file to be created.
    :return: a path to the saved image.
    """
    path = ''.join((get_absolute_root_path(), '\\output\\', directory, '\\', name, '.png'))
    cv2.imwrite(path, image)
    return path


def get_absolute_path(filename='setup.py'):
    """
    The function used to find all absolute paths to files with the given name.

    :param filename: name of file, which absolute path should be found.
    :return: list with absolute paths to files with the given name.
    """
    os.chdir(CONST_WORK_DIR)
    os.chdir(os.path.dirname(os.getcwd()))
    paths = []
    for root, dirs, files in os.walk(os.getcwd(), topdown=True):
        dirs[:] = [d for d in dirs if d != 'venv']
        for name in files:
            if name == filename:
                paths.append(os.path.abspath(os.path.join(root, name)))
    return paths


def show_image(image, width=9, height=9):
    """
    The function used to display an input image.

    :param image: an array-like or a PIL image representation.
    :param width: a width of plot (in inches).
    :param height: a height of plot (in inches).
    :return: an input image.
    """
    plt.figure(figsize=(width, height))
    plt.imshow(image, cmap='gray')
    plt.show()
    return image


def show_augmented(augmentation, image, width=9, height=9):
    """
    The function used to display an input image with specified Albu augmentation.

    :param augmentation: an Albu augmentation function.
    :param image: array-like or PIL image representation.
    :param width: a width of plot (in inches).
    :param height: a height of plot (in inches).
    :return: an augmented image.
    """
    augmented = augmentation(image=image)['image']
    show_image(augmented, width=width, height=height)
    return augmented


def read_dcm(path):
    """
    The function used to get an image from a DICOM storage file.

    :param path: an absolute path to a DICOM storage file.
    :return: an image from a DICOM storage file.
    """
    try:
        parser = dicomparser.DicomParser(path)
        image = parser.GetImage()
        image = np.asarray(image, dtype="uint8")
    except AttributeError:
        dcm = pydicom.read_file(path)
        image = apply_voi_lut(apply_modality_lut(dcm.pixel_array, dcm), dcm)
        image = ((image - image.min()) / (image.max() - image.min()) * 255).astype(np.uint8)
        image = np.invert(image)
    return image


def rotate_image(image, angle):
    """
    The function used to rotate an input image.

    :param image: an image to be rotated.
    :param angle: an angle of rotation.
    :return: rotated image.
    """
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result
